from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.views.generic import TemplateView, DeleteView

from rates import models
from rates.models import Rate
from rates.utils.forms import AddRateForm
from rates.utils.rates_api import RatesApi

from rates.utils.scheduler import refresh_rates

refresh_rates(repeat=10)


class RateView(TemplateView):
    def get(self, request, *args, **kwargs):
        user = request.user # where do we take this user
        account = models.Account.objects.get(username=user.username)# retreive all objects from the db for particular user
        rates = account.saved_rates.all()# retreive all saved rates of the user from Many to many Rel ( we eill get list of rates related to the particular user)

        # TODO: add serializer for this:
        rates = [{
            'base': r.base,
            'target': r.target,
            'price': '{:.2f}'.format(r.price)
        } for r in rates]

        return render(request, 'index.html', {
            'form': AddRateForm,
            'rates_list': rates,
            'user': account
        })

    def post(self, request):
        user = request.user
        account = models.Account.objects.get(username=user.username)

        data = request.POST
        base = data.get('base')
        target = data.get('target')
        if (base and target) and (base != target):  #?
            api = RatesApi()
            price = api.get_ticker_pair_rate(base=base, target=target) # get the latest price for new pair of currency from API
            if isinstance(price, float):
                rate, _created = models.Rate.objects.get_or_create(base=base.upper(),
                                                                   target=target.upper(),
                                                                   defaults={'price': price}) #created query to save a new currency pair
                account.saved_rates.add(rate)
                account.save()#saved
        return HttpResponseRedirect(reverse('index')) # as a respone return the page from our GET methodmain

class ClientUploadDelete(DeleteView):
    model = Rate
    success_url = reverse_lazy('dashboard')
    template_name = 'index.html'